import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ListadoComponent} from "./pages/listado/listado.component";
import {ComprarComponent} from "./pages/comprar/comprar.component";
import {BuscarComponent} from "./pages/buscar/buscar.component";
import {CursoComponent} from "./pages/curso/curso.component";
import {PrincipalComponent} from "./pages/principal/principal.component";

const routes: Routes = [
  {
    path: '',
    component: PrincipalComponent,
    children: [
      {
        path: 'listado',
        component: ListadoComponent
      },
      {
        path: 'comprar',
        component: ComprarComponent
      },
      {
        path: 'buscar',
        component: BuscarComponent
      },
      {
        path: ':id',
        component: CursoComponent
      },
      {
        path: '**',
        redirectTo: 'listado'
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class CursosRoutingModule {
}
