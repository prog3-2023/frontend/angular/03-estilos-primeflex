import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Curso} from "../interfaces/curso.interface";

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  constructor(private httpClient: HttpClient) {
  }

  getCursos() {
    return this.httpClient.get<Curso[]>('http://localhost:8080/cursos');
  }
}
